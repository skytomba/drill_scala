import scala.io.StdIn

object e_2_1_1 extends App {
  val user_name = StdIn.readLine()
  println(s"Hi, $user_name!")
}
